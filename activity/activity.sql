/*

	MINI CAPSTONE

	1. Return the customerName of the customers who are from the Philippines
	2. Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"
	3. Return the product name and MSRP of the product named "The Titanic"
	4. Return the first and last name of the employee whose email is "ifirrelli@classicmodelcars.com"
	5. Return the names of customers who have no registered state
	6. Return the first name, last name, email of the employee whose last name is Patterson and first name is
	Steve
	7. Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000
	8. Return the customer numbers of orders whose comments contain the string 'DHL'
	9. Return the product lines whose text description mentions the phrase 'state of the art'
	10. Return the countries of customers without duplication
	11. Return the statuses of orders without duplication
	12. Return the customer names and countries of customers whose country is USA, France, or Canada
	13. Return the first name, last name, and office's city of employees whose offices are in Tokyo
	14. Return the customer names of customers who were served by the employee named "Leslie Thompson"
	15. Return the product names and customer name of products ordered by "Baane Mini Imports"

*/

-- 1. Return the customerName of the customers who are from the Philippines
SELECT customerName
FROM customers
WHERE country = 'Philippines';

-- 	2. Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"
SELECT contactLastName, contactFirstName
FROM customers
WHERE customerName = 'La Rochelle Gifts';

-- 	3. Return the product name and MSRP of the product named "The Titanic"
SELECT productName, MSRP
FROM products
WHERE productName = 'The Titanic';

-- 	4. Return the first and last name of the employee whose email is "ifirrelli@classicmodelcars.com"
SELECT firstName, lastName
FROM employees
WHERE email = 'ifirrelli@classicmodelcars.com';

-- 	5. Return the names of customers who have no registered state
SELECT customerName
FROM customers
WHERE state IS NULL;

-- 	6. Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve
SELECT firstName, lastName, email
FROM employees
WHERE lastName = 'Patterson' AND firstName = 'Steve';

-- 	7. Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000
SELECT customerName, country, creditLimit
FROM customers
WHERE country != 'USA' AND creditLimit > 3000;

-- 	8. Return the customer numbers of orders whose comments contain the string 'DHL'
SELECT customerNumber
FROM orders
WHERE comments LIKE '%DHL%';


-- 	9. Return the product lines whose text description mentions the phrase 'state of the art'
SELECT productLine
FROM productLines
WHERE textDescription LIKE '%state of the art%';

-- 	10. Return the countries of customers without duplication
SELECT DISTINCT country
FROM customers;

-- 	11. Return the statuses of orders without duplication
SELECT DISTINCT status
FROM orders;


-- 12. Return the customer names and countries of customers whose country is USA, France, or Canada
SELECT customerName, country
FROM customers
WHERE country IN ('USA', 'France', 'Canada');


-- 13. Return the first name, last name, and office's city of employees whose offices are in Tokyo

		-- e is an alias for the employees table.
		-- o is an alias for the offices table. 

SELECT e.firstName, e.lastName, o.city
FROM employees e
-- Join with the offices table (alias: o) based on the officeCode
JOIN offices o ON e.officeCode = o.officeCode
WHERE o.city = 'Tokyo';


-- 14. Return the customer names of customers who were served by the employee named "Leslie Thompson"
	
	-- c is an alias for the customers table.
	-- e is an alias for the employees table.
	-- o is an alias for the offices table. 

SELECT DISTINCT c.customerName
FROM customers c
-- Join with the employees table (alias: e) based on salesRepEmployeeNumber
JOIN employees e ON c.salesRepEmployeeNumber = e.employeeNumber
-- Join with the orders table (alias: o) based on customerNumber
JOIN orders o ON c.customerNumber = o.customerNumber
WHERE e.firstName = 'Leslie' AND e.lastName = 'Thompson';


-- 15. Return the product names and customer name of products ordered by "Baane Mini Imports"

	-- p is an alias for the products table.
	-- c is an alias for the customers table.
	-- o is an alias for the offices table. 
	-- od is an alias for the orderdetails table. 


SELECT p.productName, c.customerName
FROM products p
-- Join orderdetails with products based on productCode
JOIN orderdetails od ON p.productCode = od.productCode
-- Join orders with orderdetails based on orderNumber
JOIN orders o ON od.orderNumber = o.orderNumber
-- Join customers with orders based on customerNumber
JOIN customers c ON o.customerNumber = c.customerNumber
-- Filter the results to customers with the name 'Baane Mini Imports'
WHERE c.customerName = 'Baane Mini Imports';

