-- [CUSTOMERS]

CREATE TABLE customers (
    customerNumber int(11) NOT NULL AUTO_INCREMENT,
    customerName varchar(50) NOT NULL,
    contactLastName varchar(50) NOT NULL,
    contactFirstName varchar(50) NOT NULL,
    phone varchar(15),
    addressLine1 varchar(50),
    addressLine2 varchar(50),
    city varchar(50),
    state varchar(50),
    postalCode varchar(10),
    country varchar(50),
    salesRepEmployeeNumber int(11),
    creditLimit decimal(10,2),
    PRIMARY KEY (customerNumber),
    FOREIGN KEY (salesRepEmployeeNumber) REFERENCES employees(employeeNumber)
);

-- [PRODUCTS]

CREATE TABLE products (
    productCode varchar(15) NOT NULL,
    productName varchar(70) NOT NULL,
    productLine varchar(50) NOT NULL,
    productScale varchar(10) NOT NULL,
    productVendor varchar(50) NOT NULL,
    productDescription text NOT NULL,
    quantityInStock smallint(6) NOT NULL,
    buyPrice decimal(10, 2) NOT NULL,
    MSRP decimal(10, 2) NOT NULL,
    PRIMARY KEY (productCode),
    INDEX (productName),
    FOREIGN KEY (productLine) REFERENCES productLines(productLine)
);



-- [EMPLOYEE]

CREATE TABLE employees (
    employeeNumber int(11) NOT NULL,
    lastName varchar(50) NOT NULL,
    firstName varchar(50) NOT NULL,
    extension varchar(10),
    email varchar(100),
    officeCode varchar(10) NOT NULL,
    reportsTo int(11),
    jobTitle varchar(50) NOT NULL,
    PRIMARY KEY (employeeNumber),
    FOREIGN KEY (officeCode) REFERENCES offices(officeCode),
    FOREIGN KEY (reportsTo) REFERENCES employees(employeeNumber)
);


-- [PRODUCT LINES]

CREATE TABLE productLines (
    productLine varchar(50) NOT NULL,
    textDescription varchar(4000),
    htmlDescription mediumtext,
    image mediumblob,
    PRIMARY KEY (productLine)
);


-- [ORDERS]

CREATE TABLE orders (
    orderNumber int(11) NOT NULL,
    orderDate date,
    requiredDate date,
    shipDate date,
    status varchar(15),
    comments text,
    customerNumber int(11) NOT NULL,
    PRIMARY KEY (orderNumber),
    FOREIGN KEY (customerNumber) REFERENCES customers(customerNumber)
);


-- [OFFICES]

CREATE TABLE offices (
    officeCode varchar(10) NOT NULL,
    city varchar(50) NOT NULL,
    phone varchar(50) NOT NULL,
    addressLine1 varchar(50) NOT NULL,
    addressLine2 varchar(50),
    state varchar(50),
    country varchar(50) NOT NULL,
    postalCode varchar(15) NOT NULL,
    territory varchar(10) NOT NULL,
    PRIMARY KEY (officeCode)
);



-- [ORDERDETAILS]

CREATE TABLE orderdetails (
    orderNumber int(11) NOT NULL,
    productCode varchar(15) NOT NULL,
    quantityOrdered int(11) NOT NULL,
    priceEach decimal(10, 2) NOT NULL,
    orderLineNumber smallint(6),
    PRIMARY KEY (orderNumber, productCode),
    FOREIGN KEY (orderNumber) REFERENCES orders(orderNumber),
    FOREIGN KEY (productCode) REFERENCES products(productCode)
);
